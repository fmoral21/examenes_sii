#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <signal.h>
#include <sys/wait.h>
#include <stdlib.h>

pid_t pid_h;

void tratar_alarma(int sig){
	kill(pid_h , SIGUSR1);
}

void tratar_alarma2(int sig){
	//if (sig == SIGUSR1)
		//printf("Se ha recibido la señal SIGUSR1\n");
	//else if (sig == SIGALRM) {
		execlp("ls", "ls", "-alt", "/tmp", NULL);
		perror("execlp");
		exit(1);
	/*}*/
}

int main (int argc, char *argv[]) {
	int status, pid;
	struct sigaction act, act2;
	
	switch(pid = fork()){
		case -1:
		perror("error en el fork()\n");
		return 1;
		
		case 0:
		
		act2.sa_handler = &tratar_alarma2;
		sigemptyset(&act2.sa_mask);
		act2.sa_flags = SA_RESTART;
		sigaction(SIGALRM, &act2, NULL);
		//sigaction(SIGUSR1, &act2, NULL);
		alarm(10);
		//sleep(15);
		pid_h = getpid();
		printf("Padre %d, hijo %d\n", getppid(),getpid());
		
		default:
		act.sa_handler = &tratar_alarma;
		sigemptyset(&act.sa_mask);
		act.sa_flags = SA_RESTART;
		sigaction(SIGALRM, &act, NULL);
		alarm(3);
		sleep(5);
		
		
	}
	wait(&status);
	if (WIFSIGNALED(status) && (WTERMSIG(status) == SIGUSR1))
			printf("Recepcion de la señal %d\n", WTERMSIG(status));
			
	return 0;
}
